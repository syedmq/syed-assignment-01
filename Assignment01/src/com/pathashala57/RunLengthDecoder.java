package com.pathashala57;
//Represents RunLength Decoder

class RunLengthDecoder {

    private static final int step = 2;
    private static final int initial_Count = 0;
    private static final int initial_Index = 1;


    String decode(String stringToBeDecoded) {
        StringBuilder uncompressedString = new StringBuilder();
        char characterToBeDecoded;
        int index = initial_Index;
        int counter = initial_Count;
        int length = stringToBeDecoded.length();
        while (length > index) {
            characterToBeDecoded = stringToBeDecoded.charAt(index);
            uncompressedString.append(getSubString(stringToBeDecoded, characterToBeDecoded, counter));
            index += step;
            counter += step;
        }
        return uncompressedString.toString();
    }

    private String getSubString(String stringToBeDecoded, char characterToBeDecoded, int counter) {
        int localCounter = getLocalCounter(stringToBeDecoded, counter);
        int iterator = initial_Count;
        StringBuilder subString = new StringBuilder();
        while (iterator < localCounter) {
            subString.append(characterToBeDecoded);
            iterator++;

        }
        return subString.toString();
    }

    private int getLocalCounter(String stringToBeDecoded, int counter) {
        return Integer.parseInt("" + stringToBeDecoded.charAt(counter));
    }
}
