package com.pathashala57;
//Encode Run length string

class RunLengthEncoder {
    int counter;


    public static final int initial_Count = 0;

    java.lang.String encode(java.lang.String stringToBeConverted) {

        counter = initial_Count;
        int lengthOfString = getLengthOfString(stringToBeConverted);
        StringBuilder compressedString = new StringBuilder();
        while (lengthOfString > counter) {

            compressedString.append(subString(stringToBeConverted));
        }
        return compressedString.toString();
    }

    private int getLengthOfString(String stringToBeConverted) {
        return stringToBeConverted.length();
    }

    java.lang.String subString(java.lang.String stringToBeConverted){
        StringBuilder compressedSubString = new StringBuilder();
        int lengthOfString = getLengthOfString(stringToBeConverted);
        char characterBeingDecoded = stringToBeConverted.charAt(counter);
        int localCountOfCharacter = initial_Count;
        while (sameCharacter(stringToBeConverted, lengthOfString, characterBeingDecoded)) {
            localCountOfCharacter++;
            counter++;
        }
        compressedSubString.append(localCountOfCharacter);
        compressedSubString.append(characterBeingDecoded);


        return compressedSubString.toString();
    }

    private boolean sameCharacter(String stringToBeConverted, int lengthOfString, char characterBeingDecoded) {
        return lengthOfString > counter && characterBeingDecoded == stringToBeConverted.codePointAt(counter);
    }

}
