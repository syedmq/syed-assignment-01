package com.pathashala57;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestRunLengthEncoder {
    @Test
    void returnsSingleCharacterAsItIs(){

        RunLengthEncoder runLengthEncoder = new RunLengthEncoder();
        assertEquals("1C",runLengthEncoder.encode("C"));
    }

    @Test
    void returnsCompressedSingleCharacterString(){

        RunLengthEncoder runLengthEncoder = new RunLengthEncoder();
        assertEquals("3C",runLengthEncoder.encode("CCC"));
    }

    @Test
    void returnsCompressedStringEvenForStringsWithDifferentCharacters(){

        RunLengthEncoder runLengthEncoder = new RunLengthEncoder();
        assertEquals("3C1B2 2F2f",runLengthEncoder.encode("CCCB  FFff"));
    }

    @Test
    void returnsSingleUncompressedCharacters(){

        RunLengthDecoder decoder= new RunLengthDecoder();
        assertEquals("C",decoder.decode("1C"));
    }

    @Test
    void returnsUncompressedString(){

        RunLengthDecoder decoder= new RunLengthDecoder();
        assertEquals("CCBBDD",decoder.decode("2C2B2D"));
    }

    @Test
    void returnsUncompressedStringWithSpaces(){

        RunLengthDecoder decoder= new RunLengthDecoder();
        assertEquals("CCBB  DD",decoder.decode("2C2B2 2D"));
    }
}
